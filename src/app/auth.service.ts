import { Injectable, Optional, NgZone } from '@angular/core';
import { Observable, timer, of, forkJoin } from 'rxjs';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { HttpInterceptor, HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpRequest } from "@angular/common/http";
import { HttpHandler } from "@angular/common/http";
import { map, concatMap, share, takeUntil } from 'rxjs/operators';

const REFRESH_TOKEN_LIFETIME = 1000 * 60 * 10; // ten minutes

export class ApplicationSettings {
  applicationName = 'WebPlanner';
  registrationEnabled = false;
}

export class AngstremSettings {
  priceFtp: string;
}

export class PlannerSettings {
  showPrices = false;
  allowExport = false;
  sounds = true;
  showFavoriteFolders = false;
  modelFolders: number[];
}

export class EmbeddedSettings {
  enabled = true;
  userName: string;
  configUrl?: string; // can be used in addition to query params
  priceUrl?: string;
  orderUrl?: string;
}

export class OrderSettings {
  enabled = true;
  statuses: string[];
}

export interface UserUpdateData {
  id?: number;
  username: string;
  fullname?: string;
  email?: string;
  password?: string;
  externalId?: string;
  parentUserId?: number;
  // use to update parentUserId if it is not known yet
  parentUserName?: string;
  address?: string;
  phone?: string;
  roles?: string[];
}

export interface UserInfo {
  id: number;
  name: string;
  fullName: string;
  email: string;
  emailConfirmed: boolean;
  firm?: { id: string; name: string };

  admin: boolean;
  member: boolean;
  client: boolean;
  roles: string[];

  externalId: string;
  parentUserId?: number;
  employees: UserInfo[]
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  accessToken?: string;
  isAuthenticated = new BehaviorSubject<boolean | undefined>(undefined);
  isAdmin = new BehaviorSubject<boolean | undefined>(undefined);
  userName = '';
  address = '';
  phone = '';
  activePriceListId: number;
  fullName = '';
  email = '';
  roles: string[] = [];
  employees: UserInfo[] = [];
  admin = false;
  superAdmin = false;
  firm: { id: number; name: string };
  angstrem: boolean;
  settings = new ApplicationSettings();
  embedded = new EmbeddedSettings();

  private refreshToken: string;
  private authAction$ = new Subject<void>();
  private storeAuthTokens = true;

  constructor(private http: HttpClient, private zone: NgZone) {
    let host = window.location.hostname;
    this.angstrem = host.indexOf('localhost') >= 0 || host.indexOf('angstrem') >= 0
    this.refreshToken = localStorage.getItem('refresh_token');
    // protection against token written as 'undefined' to storage
    if (this.refreshToken && this.refreshToken.length < 10) {
      this.refreshToken = undefined;
    }
    setTimeout(_ => this.updateRefreshToken());
    this.zone.runOutsideAngular(() => {
      timer(REFRESH_TOKEN_LIFETIME, REFRESH_TOKEN_LIFETIME).subscribe(_ =>
        this.updateRefreshToken()
      );
    });
    // to avoid cyclic dependency with AuthInterceptor
    setTimeout(_ =>
      this.getAppSetting<ApplicationSettings>('ApplicationSettings')
      .subscribe(v => this.settings = v));
  }

  private resetAuth() {
    this.userName = '';
    this.fullName = '';
    this.address = '';
    this.phone = '';
    this.activePriceListId = undefined;
    this.email = '';
    this.accessToken = undefined;
    this.refreshToken = undefined;
    this.roles = [];
    this.firm = undefined;
    this.admin = false;
    this.superAdmin = false;
    this.isAuthenticated.next(false);
    this.isAdmin.next(false);
  }

  private processLogin() {
    if (this.accessToken && this.refreshToken) {
      let obs = this.http.post<any>('/api/account/userinfo', { full: true }).pipe(share());
      obs.subscribe(response => {
        this.userName = response.name;
        this.fullName = response.fullName;
        this.address = response.address;
        this.phone = response.phone;
        this.activePriceListId = response.activePriceListId;
        this.email = response.email;
        this.roles = response.roles;
        this.employees = response.employees || [];
        this.firm = response.firm;
        this.admin = this.roles.includes('admin');
        this.superAdmin = this.roles.includes('superadmin');
        if (this.isAuthenticated.value !== true) {
          this.isAuthenticated.next(true);
        }
        this.isAdmin.next(this.admin);
        this.authAction$.next();
        return true;
      },
      error => {
        this.resetAuth();
        return 'access_token is invalid';
      });
      return obs;
    }
    return of(false);
  }

  private encodeParams(params: any): string {
    let body = '';
    for (let key in params) {
      if (body.length) {
        body += '&';
      }
      body += key + '=';
      body += encodeURIComponent(params[key]);
    }

    return body;
  }

  login(info: { userName: string; password: string }) {
    let params = {
      client_id: 'WebPlanner',
      grant_type: 'password',
      username: info.userName,
      password: info.password,
      scope: 'WebAPI offline_access openid profile roles'
    };
    let body = this.encodeParams(params);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    return this.http
      .post<any>('connect/token', body, { headers }).pipe(
      concatMap(response => {
        this.accessToken = response.access_token;
        this.refreshToken = response.refresh_token;
        this.saveAuthToStorage();
        return this.processLogin();
      }));
  }

  private updateRefreshToken() {
    if (!this.refreshToken) {
      if (this.isAuthenticated.value !== false) {
        this.resetAuth();
      }
      return;
    }
    let params: any = {
      client_id: 'WebPlanner',
      grant_type: 'refresh_token',
      refresh_token: this.refreshToken
    };

    let body: string = this.encodeParams(params);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    this.http
      .post<any>('connect/token', body, { headers })
      .pipe(takeUntil(this.authAction$))
      .subscribe(body => {
        if (body.access_token && body.refresh_token) {
          // Stores access token & refresh token.
          this.accessToken = body.access_token;
          this.refreshToken = body.refresh_token;
          this.saveAuthToStorage();
          this.processLogin();
        } else {
          this.resetAuth();
        }
      }, error => this.resetAuth());
  }

  loginAs(id: number, remember = false) {
    this.storeAuthTokens = remember;
    return this.http.post<any>(`/api/account/loginas/${id}`, {})
      .pipe(concatMap(result => {
        this.accessToken = result.accessToken;
        this.refreshToken = result.refreshToken;
        this.authAction$.next();
        return this.processLogin();
      }));
  }

  embedLogin() {
    this.storeAuthTokens = true;
    let login$ = this.http.post<any>(`/api/account/embedlogin`, {});
    let embedded$ = this.getAppSetting<EmbeddedSettings>('EmbeddedSettings');
    return forkJoin(login$, embedded$).pipe(
      concatMap(result => {
        this.accessToken = result[0].accessToken;
        this.refreshToken = result[0].refreshToken;
        this.embedded = result[1];
        this.authAction$.next();
        return this.processLogin();
      }));
  }

  requestEmailToken(email: string) {
    return this.http.post<any>(`/api/account/emailtoken`, {email});
  }

  emailLogin(email: string, token: string) {
    this.storeAuthTokens = true;
    return this.http.post<any>(`/api/account/emaillogin`, {email, token}).pipe(
      concatMap(result => {
        this.accessToken = result.accessToken;
        this.refreshToken = result.refreshToken;
        this.authAction$.next();
        return this.processLogin();
      }));
  }

  register(info: { userName: string; email: string; password: string }) {
    let body: UserUpdateData = {
      username: info.userName,
      password: info.password,
      email: info.email
    };
    return this.http
      .post<any>('/api/account/register', body)
      .pipe(map(r => {
        if (!r.succeeded) {
          throw r;
        }
        return r;
      }));
  }

  logout() {
    if (this.refreshToken) {
      let params: any = {
        client_id: 'WebPlanner',
        token: this.refreshToken,
        token_type_hint: 'refresh_token'
      };
      // Encodes the parameters.
      let body: string = this.encodeParams(params);
      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post('connect/revocation', body, { headers }).subscribe();
    }
    this.authAction$.next();
    this.resetAuth();
    this.saveAuthToStorage();
  }

  private saveAuthToStorage() {
    if (this.storeAuthTokens) {
      if (this.refreshToken) {
        localStorage.setItem('refresh_token', this.refreshToken);
      } else {
        localStorage.removeItem('refresh_token');
      }
    }
  }

  getAppSetting<T>(name: string): Observable<T> {
    return this.http.get<T>(`api/account/appsetting/${name}`);
  }

  getAppSettingRaw(name: string) {
    return this.http.get<any>(`api/account/appsetting/${name}`);
  }

  setAppSetting(settings: Object, name: string) {
    return this.http.post('api/account/appsetting', {
      name,
      value: JSON.stringify(settings)
    });
  }

  setAppSettingRaw(name: string, value) {
    return this.http.post('api/account/appsetting', {
      name,
      value: JSON.stringify(value)
    });
  }
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ) {
    if (this.auth && this.auth.accessToken) {
      let cors = req.url.match(/https{0,1}:/);
      if (!cors) {
        req = req.clone({
            setHeaders: { 'Authorization': 'Bearer ' + this.auth.accessToken }
          });
      }
    }
    return next.handle(req);
  }
}
