import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { map, concatMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  error$: Observable<any>;

  constructor(private auth: AuthService, private route: ActivatedRoute, router: Router) {
    this.error$ = route.params.pipe(
      map(p => p['token']),
      concatMap(token => auth.emailLogin('Rem.Kolomna@gmail.com', token as string)),
      tap(ok => router.navigate(['/files'])),
      catchError(e => of(true))
    );
  }

  ngOnInit() {
  }

}
