import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowseComponent } from './browse/browse.component';
import { FilesComponent } from './files/files.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthComponent } from './auth/auth.component';

let routes: Route[] = [
  { path: 'login', component: LoginComponent},
  { path: 'auth/:token', component: AuthComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BrowseComponent,
    FilesComponent,
    LoginComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(routes), HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
